var n = 7;

function fib(liczba) {
    var a = 1,
        b = 0,
        tmp;

    while (liczba >= 0) {
        tmp = a;
        a = a + b;
        b = tmp;
        liczba--;
    }

    return b;
}
console.log("Wynik algorytmu Fibonacciego dla liczby " + n)
console.log(fib(n));