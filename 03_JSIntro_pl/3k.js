var student = {
    imie: "Jan",
    nazwisko: "Kowalski",
    nrIndeksu: 12345,
    fPrzedstawSie: function() {
        return "Nazywam się" + this.imie + " " + this.nazwisko
    },
    fPodajNrIndeksu: function() {
        return this.nrIndeksu;
    },
    fZmienNrIndeksu: function(nowyNr) { this.nrIndeksu = nowyNr }
}

function pokazStudent(student) {
    Object.keys(student).forEach(function(wlasciwosc) {

        console.log(wlasciwosc + ":", student[wlasciwosc], "[typ " + typeof(student[wlasciwosc]) + "]");

    });
}

pokazStudent(student);