var tablica = [6, 3, 4, 5, 1, 0, 2, 5, 7, 11, 3, 6, 23, 54, 23, 65, 23, 24];

//Tablica po posortowaniu [ 0, 1, 2, 3, 3, 4, 5, 5, 6, 6, 7, 11, 23, 23, 23, 24, 54, 65 ]

function zwrocLiczby(tab) {
    tab = tab.sort((a, b) => a - b); //ECMA Script 6
    //console.log("tablica po posortowaniu:")
    //console.log(tab);
    return "Druga najmniejsza: " + tab[1] + "; Druga największa: " + tab[tab.length - 2];
}
console.log("W tablicy [" + tablica + "] szukam drugiej najmniejszej i drugiej największej liczby");
console.log(zwrocLiczby(tablica));

/*wynik:
W tablicy [6,3,4,5,1,0,2,5,7,11,3,6,23,54,23,65,23,24] szukam drugiej najmniejszej i drugiej największej liczby
Druga najmniejsza: 1; Druga największa: 54
*/