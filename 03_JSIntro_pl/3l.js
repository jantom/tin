function Student(imie, nazwisko, nrIndeksu, oceny) {
    this.imie = imie;
    this.nazwisko = nazwisko;
    this.nrIndeksu = nrIndeksu;
    this.oceny = oceny;
}

var student1 = new Student("Jan", "Kowalski", "12345", [3, 4, 5, 3]);
var student2 = new Student("Aleks", "Iksinski", "2345", [3, 3, 3, 4]);

function pokazStudent(student) {
    console.log("Student: " + student.imie + " " + student.nazwisko);
    console.log("Numer indeksu: " + student.nrIndeksu);
    console.log("Srednia ocen: " + student.oceny.reduce((a, b) => a + b, 0) / student.oceny.length);
}

pokazStudent(student1);
pokazStudent(student2);