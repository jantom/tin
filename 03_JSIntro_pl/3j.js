function binarySearch(tablica, wartosc) {

    var min = 0,
        max = tablica.length - 1,
        znacznik = Math.floor((max + min) / 2);

    while (tablica[znacznik] != wartosc && min < max) {
        if (wartosc < tablica[znacznik]) {
            max = znacznik - 1;
        } else if (wartosc > tablica[znacznik]) {
            min = znacznik + 1;
        }
        znacznik = Math.floor((max + min) / 2);
    }
    return (tablica[znacznik] != wartosc) ? -1 : znacznik;
}

tablica = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
console.log(binarySearch(tablica, 1));