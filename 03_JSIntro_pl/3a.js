var n = 7;

var silniaRek = function(liczba) {
    if (liczba < 2) {
        return liczba;
    } else {
        return liczba * silniaRek(liczba - 1);
    }
}

function silniaIter(liczba) {
    var wynik = 1;
    for (var i = 1; i <= liczba; i++) {
        wynik *= i;
    }
    return wynik;
}

console.log("Silnia rekurencyjnie dla liczby " + n);
console.log(silniaRek(n));
console.log("Silnia iteracyjnie dla liczby " + n);
console.log(silniaIter(n));