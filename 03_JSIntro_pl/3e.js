var ciag = "Lorem ipsum dolor sit amet, consectetur adipiscing elit";

function znajdzNajdluzszeSlowo(ciag) {
    var najdluzszeSlowo = ciag.split(' ').sort(function(a, b) { return b.length - a.length; });
    return najdluzszeSlowo[0];
}

console.log("Najdluzsze slowo w ciagu:");
console.log(ciag);
console.log("to:");
console.log(znajdzNajdluzszeSlowo(ciag));