function amountTocoins(kwota, monety) {
    this.kwota = kwota;
    this.monety = monety;
    this.wynik = '';

    for (var i = 0; i < monety.length; i++) {
        if (kwota >= monety[i]) {
            kwota = kwota - monety[i];
            wynik = wynik + monety[i] + ', ';
            i--;
        }
    }
    return wynik.slice(0, wynik.length - 2);
}

console.log(amountTocoins(46, [25, 10, 5, 2, 1]));