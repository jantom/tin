var http = require('http');
var url = require('url');
var port = 8000;

// Configure our HTTP server to respond with Hello World to all requests.
var server = http.createServer(function(request, response) {
    var adres = url.parse(request.url, true).query;
    response.writeHead(200, {
        "Content-Type": "text/html"
    });
    var wynik = 0;
    console.log(request.url);
    if (adres.op && isNaN(adres.l1) === false && isNaN(adres.l2) === false) {
        var a = Number(adres.l1);
        var b = Number(adres.l2);


        if (adres.op === "sum") {
            //składnia: http://localhost:port/?op=sum&l1=1&l2=1
            wynik = a + b;

            response.end('Wybrana operacja: dodawanie<br />' + a + " + " + b + " = " + wynik);
        } else if (adres.op === "div") {
            //składnia: http://localhost:port/?op=div&l1=1&l2=1
            if (b != 0 || b != 0.0) {
                wynik = a / b;
                response.end('Wybrana operacja: dzielenie<br />' + a + "/" + b + " = " + wynik);
            } else {
                response.end('Nie mozna dzielic przez 0');
            }

        } else if (adres.op === "mul") {
            //składnia: http://localhost:port/?op=div&l1=1&l2=1
            wynik = a * b;
            response.end('Wybrana operacja: mnozenie<br />' + a + "*" + b + " = " + wynik);
        } else {
            response.end('Kalkulator nie obsluguje wybranej operacji');
        }
    } else {
        if (request.url === '/') {
            response.end("<h1>Prosty kalkulator z serwera node.js</h1><p>Kalkulator pracuje na liczbach stalo i zmiennoprzecinkowych. Aby dokonac obliczen, podaj w url nastepujace parametry:</p><ol><li>op - mozliwe parametry, kolejno dla dodawania, dzielenia, mnozenia:<ol><li>sum</li><li>div</li><li>mul</li></ol></li><li>l1 - pierwsza liczba</li><li>l2 - druga liczba</li></ol><h2>Przykladowe zapytanie</h2><p><a href=\"http://localhost:" + port + "/?op=sum&l1=2&l2=2\">http://localhost:" + port + "/?op=sum&l1=2&l2=2</a> - dodawanie 2+2</p><p><a href=\"http://localhost:" + port + "/?op=div&l1=2&l2=2\">http://localhost:" + port + "/?op=div&l1=2&l2=2</a> - dzielenie (2*2) WAZNE: Pamietaj, ze nie mozna dzielic przez 0!</p><p><a href=\"http://localhost:" + port + "/?op=mul&l1=2&l2=2\">http://localhost:" + port + "/?op=mul&l1=2&l2=2</a> - mnozenie (2/2)</p>");
        }
        if (adres.op === '') {
            response.end("Nie podano rodzaju operacji matematycznej");
        }
        if (isNaN(adres.l1)) {
            response.end("Wartosc parametru dla pierwszej liczby nie jest liczbowa!");
        }
        if (isNaN(adres.l2)) {
            response.end("Wartosc parametru dla drugiej liczby nie jest liczbowa!");
        }

    }
});

server.listen(port);
console.log("serwer nasluchuje pod adresem: http://localhost:" + port);