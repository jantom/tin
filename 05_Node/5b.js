var bodyParser = require('body-parser');
var express = require('express');
var app = express();
var port = 8000;
app.use(bodyParser.json());
app.set('view engine', 'ejs');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(__dirname + '/public'));

app.get('/hello', function(req, res) {
    res.send('Hello World');
})

app.get('/form', function(req, res) {
    res.sendFile(__dirname + `/public/form.html`);
})

app.post('/formdata', function(req, res) {
    const zapytaniePost = req.body;
    console.log(req.body);
    res.render(__dirname + `/public/submit.ejs`, { login: zapytaniePost.login, nrIndeksu: zapytaniePost.nrIndeksu, email: zapytaniePost.email });
})

app.post('/jsondata', function(req, res) {
    const zapytaniePost = req.body;
    console.log(req.body);
    res.render(__dirname + `/public/submit.ejs`, { login: zapytaniePost.login, nrIndeksu: zapytaniePost.nrIndeksu, email: zapytaniePost.email });
})

var server = app.listen(port, function() {
    var host = server.address().address
    var port = server.address().port

    console.log("serwer uruchomiony pod adresem http://localhost:" + port)
})