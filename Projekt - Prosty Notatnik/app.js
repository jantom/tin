const express = require('express');
const fileUpload = require('express-fileupload');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const path = require('path');

const app = express();

const {pokazNotatki} = require('./routes/index');
const {dodajNotatkeStrona, dodajNotatke, usunNotatke, zmienNotatke, zmienNotatkeStrona} = require('./routes/notatka');

const port = 8000;

// create connection to database
// the mysql.createConnection function takes in a configuration object which contains host, user, password and the database name.
const db = mysql.createConnection ({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'socka'
});

// connect to database
db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database');
});
global.db = db;

// configure middleware
app.set('port', process.env.port || port); // set express to use this port
app.set('views', __dirname + '/views'); // set express to look in this folder to render our view
app.set('view engine', 'ejs'); // configure template engine
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.text({ type: 'text/html' }))
app.use(bodyParser.text({ type: 'text/xml' }))
app.use(bodyParser.raw({ type: 'application/vnd.custom-type' }))
app.use(bodyParser.json({ type: 'application/*+json' }))
app.use(express.static(path.join(__dirname, 'public'))); // configure express to use public folder

// routes for the app

app.get('/', pokazNotatki);
app.get('/dodaj', dodajNotatkeStrona);
app.get('/zmien/:id', zmienNotatkeStrona);
app.get('/usun/:id', usunNotatke);
app.post('/dodaj', dodajNotatke);
app.post('/zmien/:id', zmienNotatke);

// set the app to listen on the port
app.listen(port, () => {
    console.log(`Serwer działa pod adrese: http://localhost:${port}`);
});