module.exports = {
    pokazNotatki: (req, res) => {
        let query = "SELECT * FROM `notatka` ORDER BY nr ASC"; // query database to get all the players

        // execute query
        db.query(query, (err, result) => {
            if (err) {
                res.redirect('/');
            }
            res.render('index.ejs', {
                title: "Mini notatnik"
                ,notatka: result
            });
        });
    },
};