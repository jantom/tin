const fs = require('fs');
const sanitizeHtml = require('sanitize-html');
module.exports = {
    dodajNotatkeStrona: (req, res) => {
        res.render('dodajNotatke.ejs', {
            title: "Dodaj nową notatkę"
            ,message: ''
        });
    },
    dodajNotatke: (req, res) => {
        let message = '';
        let tytul = req.body.tytul;
        let tresc = req.body.tresc;
        let kolor = req.body.kolor;
        let zapytanie = "SELECT * FROM `notatka` WHERE tytul = '" + tytul + "'";

        console.log(req);

        db.query(zapytanie, (err, result) => {
            if (err) {
                return res.status(500).send(err);
            }
            if (result.length > 0) {
                message = 'Username already exists';
                res.render('dodajNotatke.ejs', {
                    message,
                    title: "Dodaj nową notatkę"
                });
            } else {
               
                        let zapytanie = "INSERT INTO `notatka` (`nr`, `tytul`, `tresc`, `dataDodania`, `dataModyfikacji`, `kolor`, `img`) VALUES (NULL, '"+tytul+"', '"+tresc+"', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '"+kolor+"', '')";
                        console.log(zapytanie);
                        db.query(zapytanie, (err, result) => {
                            if (err) {
                                return res.status(500).send(err);
                            }
                            res.redirect('/');
                        });
               
                }
        });
    },
    zmienNotatkeStrona: (req, res) => {
        let Nnr = req.params.id;
        let zapytanie = "SELECT * FROM `notatka` WHERE nr = '" + Nnr + "' ";
        db.query(zapytanie, (err, result) => {
            if (err) {
                return res.status(500).send(err);
            }
            res.render('edytujNotatke.ejs', {
                title: "Zaktualizuj notatkę"
                ,notatka: result[0]
                ,message: ''
            });
        });
    },
    zmienNotatke: (req, res) => {
        let Nnr = req.params.id;
        let tytul = req.body.tytul;
        let tresc = req.body.tresc;
        let kolor = req.body.kolor;

        let zapytanie = "UPDATE `notatka` SET `tytul` = '" + tytul + "', `tresc` = '" + tresc + "', `dataModyfikacji` = CURRENT_TIMESTAMP, `kolor` = '"+kolor+"' WHERE `nr` = '" + Nnr + "'";
        db.query(zapytanie, (err, result) => {
            if (err) {
                return res.status(500).send(err);
            }
            res.redirect('/');
        });
    },
    usunNotatke: (req, res) => {
        let Nnr = req.params.id;
        let zapytanie = 'DELETE FROM notatka WHERE nr = "' + Nnr + '"';
        console.log(req);
        console.log(res);
        console.log(zapytanie);
                db.query(zapytanie, (err, result) => {
                    if (err) {
                        return res.status(500).send(err);
                    }
                    res.redirect('/');
                });
    }
};
